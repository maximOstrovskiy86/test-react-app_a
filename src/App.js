import React from "react";
import './App.scss';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

import Main from "./components/Main"
import Create from "./components/Create";
import Edit from "./components/Edit";


function App() {
  return (
    <>
        <Router>
            <Switch>
                <Route exact path="/" component={Main} />
                <Route path="/create" component={Create} />
                <Route path="/edit/:id" component={Edit} />
            </Switch>
        </Router>
    </>
  );
}

export default App;
