import React, {useEffect, useState} from "react";
import {useParams, useHistory} from 'react-router-dom'

function Edit() {

    let history = useHistory();
    const [invoice, setInvoice] = useState([])
    const {id} = useParams();

    const readOnly = () => {
        history.push("/")
    }

    useEffect(() => {
        async function editData() {
            try {
                const response = await fetch(`http://localhost:3000/invoices/${id}`);
                const invoice = await response.json();
                setInvoice(invoice);
                console.log(invoice);
            } catch (error) {
                console.error(error);
            }
        }
        editData()
    }, [id])

    if(!invoice) {
        return null
    }

    return (
    <div className="container">
        <div className="invoices-field">
            <h1>Create invoice</h1>
            <form id="form">
                <label htmlFor="number">Number:<input  type="text" id="number" defaultValue={invoice.number}/></label>
                <label htmlFor="date_created">Invoice Date:<input id="date_created" defaultValue={invoice.date_created} type="date" placeholder="Select date"/></label>
                <label htmlFor="date_supplied">Supply Date:<input id="date_supplied" defaultValue={invoice.date_supplied} type="date" placeholder="Select date"/></label>
                <label htmlFor="note">Comment:<textarea id="note" defaultValue={invoice.comment} rows="4"></textarea></label>
                <button type="submit"  onClick={readOnly} className="btn">Save</button>
            </form>
        </div>
    </div>
    )
}

export default Edit;