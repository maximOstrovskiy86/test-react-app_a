import React, {useState, useEffect} from 'react';
import {Link} from "react-router-dom";
import Invoices from "./Invoices";

function Main() {

    const [data, setData] = useState([]);

    const onDelete = async (id) => {
        try {
            await fetch(`http://localhost:3000/invoices/${id}`, {
                method: 'DELETE',
            })
            setData(data.filter((item) => item.id !== id))
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        async function mainData() {
            const response = await fetch("http://localhost:3000/invoices");
            const invoices = await response.json();
            setData(invoices)
        }
        mainData()
    }, [])


    return (
        <>
            <div className="container">
                <h1>Invoices</h1>
                <div className="row top_row">
                    <h4>Actions</h4>
                    <Link to="/create" className="btn">Add new</Link>
                </div>
                <div className="row invoices_block">
                    <h4>Invoices</h4>
                    <table>
                        <tbody>
                        <tr>
                            <th>Create</th>
                            <th>No</th>
                            <th>Supply</th>
                            <th>Comment</th>
                            <th className="view">View</th>
                            <th className="delete">Delete</th>
                        </tr>
                        {data.map(item =>
                            <Invoices
                                onDelete={onDelete}
                                number={item.number}
                                created={item.date_created}
                                supplied={item.date_supplied}
                                comment={item.comment}
                                id={item.id}
                                key={item.id}/>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}

export default Main;