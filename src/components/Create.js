import React from 'react';
import {useHistory} from "react-router-dom";
import {useForm} from "react-hook-form";

function Create() {

    const {register, handleSubmit} = useForm();
    let history = useHistory();

    const onSubmit = async (data) => {
        try {
            await fetch('http://localhost:3000/invoices', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            history.push("/")
        } catch (error) {
            console.error(error)
        }
        console.log(data)
    }

    return (
        <div className="container">
            <div className="invoices-field">
                <h1>Create invoice</h1>
                <form id="form" onSubmit={handleSubmit(onSubmit)}>
                    <label htmlFor="number">Number:<input {...register('number', {required: true, minLength: 3, maxLength: 12 })} type="text"id="number"/></label>
                    <label htmlFor="date_created">Invoice Date:<input {...register('date_created',{required: true})} id="date_created" type="date" placeholder="Select date"/></label>
                    <label htmlFor="date_supplied">Supply Date:<input {...register('date_supplied', {required: true})} id="date_supplied" type="date" placeholder="Select date"/></label>
                    <label htmlFor="note">Comment:<textarea {...register('comment', { maxLength: 160 })} id="note" rows="4"></textarea></label>
                    <button type="submit" className="btn">Save</button>
                </form>
            </div>
        </div>
    )
}

export default Create;