import {useHistory} from "react-router-dom";

function Invoices(props) {

    let history = useHistory();

    const onEdit = () => {
        history.push(`/edit/${props.id}`);
    }

    return(
    <>
        <tr>
            <td>{props.created}</td>
            <td>{props.number}</td>
            <td>{props.supplied}</td>
            <td>{props.comment}</td>
            <td className="view"><span onClick={onEdit}>&#128065;</span></td>
            <td className="delete"><span  onClick={() => props.onDelete(props.id)}>&times;</span></td>
        </tr>
    </>
    )
}

export default Invoices;